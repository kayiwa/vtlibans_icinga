## Setup

This mostly works. If you are testing this. You will need [Vagrant](https://www.vagrantup.com/) and [Ansible](http://www.ansible.com/) installed on Linux or Mac OSX. The completed installation will have icinga2 and icinga2web configured on Ubuntu trusty LTS.

### Follow the steps in order

Clone this repo

Change into the directory you just cloned

`cd vtlibans_icinga`

copy the vars TEMPLATE file

`cp vars.TEMPLATE.yml vars.yml`

You can change the password and username of the resulting vars.yml which will be used in the next steps.

If you intend to run this on production you can limit to the host you plan to install it on by running the playbook.

`ansible-playbook --limit 1.2.3.4 icingaserver.yml -b`

If you intend to test this as a VM (I recommend running this a couple of times before you run on production) run

`vagrant up`

In both cases you will need to log into the host to complete installation.

In the production case run the following

`ssh user@1.2.3.4`

and in the Vagrant case run

`vagrant ssh`

Once logged in you will need to generate a token as root

`sudo icingacli setup token create`

Copy the resulting generated code into your web browser at

http://1.2.3.4/icingaweb2/setup in the case of the production one and

http://192.168.15.40/icingaweb2/setup in the case of the Vagrant one.
